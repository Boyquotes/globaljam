extends Navigation2D

export(float) var CHARACTER_SPEED = 200.0
var path = []

onready var player = get_parent().get_node("player")
onready var positionDeSpawn = get_parent().get_node("positionDeSpawn")

var fantomeInstance = load("res://entities/presences/fantome/fantome.tscn")

var nextSpawnTime = 5;
var spawnTime = 2 #5
var aide_spawn = float(0)

func _ready():
	set_process(true)
	
	randomize()
	nextSpawnTime = spawnTime

func _update_navigation_path(start_position, end_position):
	# get_simple_path is part of the Navigation2D class
	# it returns a PoolVector2Array of points that lead you from the
	# start_position to the end_position
	path = get_simple_path(start_position, end_position, true)
	# The first point is always the start_position
	# We don't need it in this example as it corresponds to the character's position
	path.remove(0)
	set_process(true)


func _process(delta):
	var positionPlayer = Vector2(player.position.x, player.position.y)
	if Input.is_action_just_pressed("fantome"):
		var fantome = fantomeInstance.instance()
		fantome.position = positionDeSpawn.position
		add_child(fantome)
		_update_navigation_path(fantome.position, positionPlayer)
		var walk_distance = CHARACTER_SPEED * delta
		move_along_path(walk_distance, fantome)


func move_along_path(distance, fantome):
	var last_point = fantome.position
	for index in range(path.size()):
		var distance_between_points = last_point.distance_to(path[0])
		# the position to move to falls between two points
		if distance <= distance_between_points and distance >= 0.0:
			fantome.position = last_point.linear_interpolate(path[0], distance / distance_between_points)
			break
		# the character reached the end of the path
		elif distance < 0.0:
			fantome.position = path[0]
			set_process(false)
			break
		distance -= distance_between_points
		last_point = path[0]
		path.remove(0)
