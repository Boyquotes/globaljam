extends Node2D

var mouse_position

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	mouse_position = get_local_mouse_position()
	rotation += mouse_position.angle()*0.1
	pass
