extends KinematicBody2D

# This is a demo showing how KinematicBody2D
# move_and_slide works.

const SPEED = 70
const MAX_SPEED = 200
const FRICTION = 0.1

# Member variables
#const MOTION_SPEED = 300 # Pixels/second
var mouse_position
var mouse_position_global
var motion =Vector2()
var velocity_multiplier = 1
var deja_en_marche = false

func _process(delta):
	update_motion(delta)
	move_and_slide(motion  * velocity_multiplier)   # ne pas multiplier par delta car déjà fait automatiquement
	
	

func update_motion(delta):
	look_at(get_global_mouse_position())
	
	if Input.is_action_pressed("ui_up") and not Input.is_action_pressed("ui_down"):
#		motion.y = clamp((motion.y - SPEED * delta), -MAX_SPEED, 0)
		motion.y = (motion.y - SPEED * delta)
	elif Input.is_action_pressed("ui_down") and not Input.is_action_pressed("ui_up"):
#		motion.y = clamp((motion.y + SPEED * delta), 0, MAX_SPEED)
		motion.y = (motion.y + SPEED * delta)
	else:
		motion.y = lerp(motion.y, 0, FRICTION)
	
	if Input.is_action_pressed("ui_left") and not Input.is_action_pressed("ui_right"):
#		motion.x = clamp((motion.x - SPEED * delta), -MAX_SPEED, 0)
		motion.x = (motion.x - SPEED * delta)
	elif Input.is_action_pressed("ui_right") and not Input.is_action_pressed("ui_left"):
#		motion.x = clamp((motion.x + SPEED * delta), 0, MAX_SPEED)
		motion.x = (motion.x + SPEED * delta)
	else:
		motion.x = lerp(motion.x, 0, FRICTION)

	# aucun deplacement = motion faible : pas de bruit de pas
#	if not Input.is_action_pressed("ui_left") and not Input.is_action_pressed("ui_right") and not Input.is_action_pressed("ui_up") and not Input.is_action_pressed("ui_down"):
	if motion.length()<=5:
		$bruit_marche_player.stop()
		deja_en_marche=false
	elif deja_en_marche==false :
		$bruit_marche_player.play()
		deja_en_marche=true
	# else     continue le bruit  (donc rien à faire en fait)
	
	# normaliser la vitesse (maximum en diagonale MAX_SPEED
	motion = motion.clamped(MAX_SPEED)
	print('motion')
	print(motion)


func _on_Faisceau_area_entered(area):
	if area.is_in_group("ennemis"):
		print("les ennemis sont la")
	pass # replace with function body


func _on_monCorps_area_entered(area):
	if area.is_in_group("ennemis"):
		print("game over")
		game_over()
	pass # replace with function body

func game_over():
	print('game over')
	$Camera2D/GAMEOVER_Sprite.show()
	$Camera2D/GAMEOVER_Sprite.rotation=0
	
#	$"../ecranGameover".position = position
#	$"../ecranGameover".show()
	get_tree().paused = true