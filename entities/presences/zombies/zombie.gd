extends Navigation2D

export(float) var CHARACTER_SPEED = 30.0
var path = []

var vitalite_zombie = float(5)
var timer_mort_zombie  = float(0)

onready var player = get_parent().get_node("player")

func _update_navigation_path(start_position, end_position):
	# get_simple_path is part of the Navigation2D class
	# it returns a PoolVector2Array of points that lead you from the
	# start_position to the end_position
	path = get_simple_path(start_position, end_position, true)
	# The first point is always the start_position
	# We don't need it in this example as it corresponds to the character's position
	path.remove(0)
	set_process(true)


func _process(delta):
	var positionPlayer = Vector2(player.position.x, player.position.y)
	$AnimatedSprite.play()
	_update_navigation_path(position, positionPlayer)
	var walk_distance = CHARACTER_SPEED * delta
	print(walk_distance)
	move_along_path(walk_distance)


func move_along_path(distance):
	var last_point = position
	for index in range(path.size()):
		var distance_between_points = last_point.distance_to(path[0])
		# the position to move to falls between two points
		if distance <= distance_between_points and distance >= 0.0:
			position = last_point.linear_interpolate(path[0], distance / distance_between_points)
			break
		# the character reached the end of the path
		elif distance < 0.0:
			position = path[0]
			set_process(false)
			break
		distance -= distance_between_points
		last_point = path[0]
		path.remove(0)



func _on_Area2D_area_entered(area):	
	print("booom  -  area2D entered Zombie")
	vitalite_zombie-= 1
	print(vitalite_zombie)
	$bruit_blessure_zombie.play()
	if vitalite_zombie <=0 :
		$bruit_mort_zombie.play()         
#		mettre un timer pour entendre le bruit.... *************************
		queue_free()
		
	pass # replace with function body
